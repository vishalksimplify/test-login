import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import records from '../views/recording'
import webrtc from '../views/webrtc'
import proctor from '../views/proctor'
import imageproctor from '../views/imageproctoring'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/record',
    name: 'record',
    component: records
  },
  {
    path: '/webrtc',
    name: 'webrtc',
    component: webrtc
  },
  {
    path: '/proctor',
    name: 'proctor',
    component: proctor
  },
  {
    path: '/imageproctor',
    name: 'imageproctor',
    component: imageproctor
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
